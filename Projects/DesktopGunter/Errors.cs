﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesktopGunter
{
    public class Errors
    {
        public int GrandTotal { get; set; }
        public int TotalToday { get; set; }
        public int Total404sToday { get; set; }
        public int LastHour { get; set; }
        public int LastHour404s { get; set; }
        public Top_Apps TopApps { get; set; }
    }

    public class Top_Apps
    {
        public AppName appName { get; set; }
        public ErrorCount errorCount { get; set; }
    }
 
    public class AppName
    {
        public string AppName { get; set; }
    }

    public class ErrorCount
    {
        public int ErrorCount { get; set; }
    }
}
