﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.IO;

namespace DesktopGunter
{
    public partial class DesktopGunterForm : Form
    {
        int noOfErrors = 0;
        int redErrors = 0;
        int grandTotal;
        int totalToday;
        int total404sToday;
        int lastHour;
        int lastHour404s; 
        char topAppOneName;
        char topAppTwoName;
        int topAppOneErrorCount;
        int topAppTwoErrorCount;
        string userName;
        string firstName;
        bool balloonTipShown = false;
        private Icon ico;
        Timer timerDateTime = new Timer();        
        Timer timerCheckGunter = new Timer();
        Label lblDateTime = new Label();

        public DesktopGunterForm()
        {
            InitializeComponent();

            icoGunterSystemTrayIcon.ContextMenuStrip = contextMenu;
            Image gunterBlue = Image.FromFile(Application.StartupPath + @"\Images\Background-Blue.png");
            this.BackgroundImage = gunterBlue;
            icoGunterSystemTrayIcon.Icon = new System.Drawing.Icon(Application.StartupPath + @"\Images\Gunter-Blue.ico");
            navShowDesktopGunter.Image = Image.FromFile(Application.StartupPath + @"\Images\showDesktopGunter.png");
            navHideDesktopGunter.Image = Image.FromFile(Application.StartupPath + @"\Images\hideDesktopGunter.png");
            navExitDesktopGunter.Image = Image.FromFile(Application.StartupPath + @"\Images\exitDesktopGunter.png");
            lblPaymentshieldShieldLogo.Image = Image.FromFile(Application.StartupPath + @"\Images\PaymentshieldShieldLogo.png");
            lblPaymentshieldShieldLogo.BackColor = Color.FromArgb(29, 116, 223);
            lblStatus.BackColor = Color.FromArgb(29, 116, 223);
            lblGunterStatus.BackColor = Color.FromArgb(29, 116, 223);

            string userNameWithDomain = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            string[] userNameSplitDomain = userNameWithDomain.Split('\\');
            string userNameNoDomainName = userNameSplitDomain[1];
            string userNameNoDomainNameCapitalised;

            if (userNameNoDomainName.Contains(".") == true)
            {
                string[] words = userNameNoDomainName.Split('.');
                words[0] = UppercaseWords(words[0]);
                words[1] = UppercaseWords(words[1]);
                lblUserName.Text = words[0] + ' ' + words[1];
                firstName = words[0];
            }
            else
            {
                userNameNoDomainNameCapitalised = UppercaseWords(userNameNoDomainName);
                lblUserName.Text = userNameNoDomainNameCapitalised;
                firstName = userNameNoDomainNameCapitalised;
            }            

            timerDateTime.Tick += new EventHandler(timer_Tick); // Everytime timer ticks, timer_Tick will be called
            timerDateTime.Interval = (1000) * (1);              // Timer will tick evert second
            timerDateTime.Enabled = true;                       // Enable the timer
            timerDateTime.Start();                              // Start the timer

            lblDateTime.Location = new Point(13, 100);
            lblDateTime.AutoSize = true;
            lblDateTime.Text = String.Empty;
            lblDateTime.BackColor = Color.FromArgb(115, 157, 181);
            lblDateTime.ForeColor = Color.FromArgb(255, 255, 255);
            lblDateTime.Font = new Font(lblDateTime.Font, FontStyle.Bold);

            this.Controls.Add(lblDateTime);

            //timerCheckGunter.Interval = 300000;//5 minutes
            timerCheckGunter.Interval = 10000;//10 seconds
            timerCheckGunter.Tick += new System.EventHandler(timerCheckGunter_Tick);
            timerCheckGunter.Start();
            txtNoOfErrors.Text = "0";

            CheckGunter();

        }

        void timer_Tick(object sender, EventArgs e)
        {
            lblDateTime.Text = DateTime.Now.ToString();
        }
              
        private void DesktopGunterForm_Load(object sender, EventArgs e)
        {
            ico = icoGunterSystemTrayIcon.Icon;
            webGunter2Point0.Navigate("http://google.com");
            
        }


        void DesktopGunterForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing || e.CloseReason == CloseReason.WindowsShutDown || e.CloseReason == CloseReason.ApplicationExitCall || e.CloseReason == CloseReason.TaskManagerClosing)
            {
                icoGunterSystemTrayIcon.Visible = false;
                this.Close();
            }
        }

        private void btnHideDesktopGunter_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void navShowDesktopGunter_Click(object sender, EventArgs e)
        {
            this.Show();
        }

        private void niGunterSystemTrayIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
        }

        private void navHideDesktopGunter_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void navExitDesktopGunter_Click(object sender, EventArgs e)
        {
            icoGunterSystemTrayIcon.Visible = false;
            this.Close();
        }

        private void webGunter2Point0_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            webGunter2Point0.Document.Body.Style = "zoom:50%;";
        }       

        private void btnGunter2Point0_Click(object sender, EventArgs e)
        {
            ProcessStartInfo gunterURL = new ProcessStartInfo("http://gunter2/");
            Process.Start(gunterURL);
        }

        private void btnDevelopmentWiki_Click(object sender, EventArgs e)
        {
            ProcessStartInfo devWikiURL = new ProcessStartInfo("http://gunter2/");
            Process.Start(devWikiURL);
        }

        private void btnInfrastructureWiki_Click(object sender, EventArgs e)
        {
            ProcessStartInfo infraWikiURL = new ProcessStartInfo("http://gunter2/");
            Process.Start(infraWikiURL);
        }
        private void timerCheckGunter_Tick(object sender, EventArgs e)
        {
            //Update Desktop Gunter
            CheckGunter();
        }

        private void CheckGunter()
        {   
            userName = lblUserName.Text;            
            string stringNoOfErrors = txtNoOfErrors.Text;
            int parsedNoOfErrors;

            
            

            if (stringNoOfErrors != "" && int.TryParse(txtNoOfErrors.Text, out parsedNoOfErrors))
            {
                noOfErrors = Convert.ToInt32(stringNoOfErrors);

                getJSONStats();

                noOfErrors = Convert.ToInt32(topAppOneErrorCount);
    
                if (noOfErrors >= 0) //GREEN
                {
                    lblGunterStatus.BackColor = Color.FromArgb(34, 177, 76);
                    lblStatus.BackColor = Color.FromArgb(34, 177, 76);
                    lblStatus.Text = noOfErrors + " errors in the last hour.";
                    lblPaymentshieldShieldLogo.BackColor = Color.FromArgb(34, 177, 76);
                    icoGunterSystemTrayIcon.Icon = new System.Drawing.Icon(Application.StartupPath + @"\Images\Gunter-Green.ico");
                    Image gunterGreen = new Bitmap(Application.StartupPath + @"\Images\Background.png");
                    this.BackgroundImage = gunterGreen;
                }

                if (noOfErrors >= 50) //AMBER
                {
                    lblGunterStatus.BackColor = Color.FromArgb(223, 157, 29);
                    lblStatus.BackColor = Color.FromArgb(223, 157, 29);
                    lblStatus.Text = noOfErrors + " errors in the last hour.";
                    lblPaymentshieldShieldLogo.BackColor = Color.FromArgb(223, 157, 29);
                    icoGunterSystemTrayIcon.Icon = new System.Drawing.Icon(Application.StartupPath + @"\Images\Gunter-Amber.ico");
                    Image gunterAmber = new Bitmap(Application.StartupPath + @"\Images\Background-Amber.png");
                    this.BackgroundImage = gunterAmber;
                }

                if (noOfErrors >= 100) //RED
                {
                    lblGunterStatus.BackColor = Color.FromArgb(223, 29, 29);
                    lblStatus.BackColor = Color.FromArgb(223, 29, 29);
                    lblStatus.Text = noOfErrors + " errors in the last hour.";
                    lblPaymentshieldShieldLogo.BackColor = Color.FromArgb(223, 29, 29);
                    icoGunterSystemTrayIcon.Icon = new System.Drawing.Icon(Application.StartupPath + @"\Images\Gunter-Red.ico");
                    Image gunterRed = new Bitmap(Application.StartupPath + @"\Images\Background-Red.png");
                    this.BackgroundImage = gunterRed;

                    redErrors = redErrors + 1;

                    if (redErrors == 6)
                    {
                        balloonTipShown = false;
                        redErrors = 1;
                    }

                    if (balloonTipShown != true)
                    {
                        icoGunterSystemTrayIcon.BalloonTipText = "Is there a problem with the website ... ?";
                        icoGunterSystemTrayIcon.BalloonTipTitle = "Hello there " + firstName + "!";
                        icoGunterSystemTrayIcon.ShowBalloonTip(3);
                        balloonTipShown = true;
                    }
                }

                /*if (noOfErrors == null) //BLUE
                {
                    lblGunterStatus.BackColor = Color.FromArgb(29, 116, 223);
                    lblStatus.BackColor = Color.FromArgb(29, 116, 223);
                    lblStatus.Text = "Nothing recieved from Gunter!";
                    lblPaymentshieldShieldLogo.BackColor = Color.FromArgb(29, 116, 223);
                    icoGunterSystemTrayIcon.Icon = new System.Drawing.Icon(Application.StartupPath + @"\Images\Gunter-Blue.ico");
                    Image gunterBlue = new Bitmap(Application.StartupPath + @"\Images\Background-Blue.png");
                    this.BackgroundImage = gunterBlue;

                }*/
            }
        }

        private void getJSONStats()
        {
                // read file into a string and deserialize JSON to a type
                Errors err = JsonConvert.DeserializeObject<Errors>(File.ReadAllText(@"D:\Enterprise\Development\PSL\Desktop_Gunter\Projects\DesktopGunter\stats.json"));
                
                // deserialize JSON directly from a file
                using (StreamReader file = File.OpenText(@"D:\Enterprise\Development\PSL\Desktop_Gunter\Projects\DesktopGunter\stats.json"))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    Errors errs = (Errors)serializer.Deserialize(file, typeof(Errors));
                    Top_Apps topApps = (Top_Apps)serializer.Deserialize(file, typeof(Top_Apps));
                    AppName appName = (AppName)serializer.Deserialize(file, typeof(AppName));
                    ErrorCount errorCount = (ErrorCount)serializer.Deserialize(file, typeof(ErrorCount));
                    
                    grandTotal = errs.GrandTotal;
                    totalToday = errs.TotalToday;
                    total404sToday = errs.Total404sToday;
                    lastHour = errs.LastHour;
                    lastHour404s = errs.LastHour404s;
                    topAppOneName = appName.AppName[0];
                    topAppTwoName = appName.AppName[1];
                    topAppOneErrorCount = errorCount.ErrorCount[0];
                    topAppTwoErrorCount = errorCount.ErrorCount[1];
                   

                }
            
        }

        static string UppercaseWords(string value)
        {
            char[] array = value.ToCharArray();
            // Handle the first letter in the string.
            if (array.Length >= 1)
            {
                if (char.IsLower(array[0]))
                {
                    array[0] = char.ToUpper(array[0]);
                }
            }
            // Scan through the letters, checking for spaces.
            // ... Uppercase the lowercase letters following spaces.
            for (int i = 1; i < array.Length; i++)
            {
                if (array[i - 1] == ' ')
                {
                    if (char.IsLower(array[i]))
                    {
                        array[i] = char.ToUpper(array[i]);
                    }
                }
            }
            return new string(array);
        }
    }
}
