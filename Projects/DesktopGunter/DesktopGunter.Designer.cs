﻿namespace DesktopGunter
{
    partial class DesktopGunterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DesktopGunterForm));
            this.btnHideDesktopGunter = new System.Windows.Forms.Button();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.navShowDesktopGunter = new System.Windows.Forms.ToolStripMenuItem();
            this.navHideDesktopGunter = new System.Windows.Forms.ToolStripMenuItem();
            this.navExitDesktopGunter = new System.Windows.Forms.ToolStripMenuItem();
            this.icoGunterSystemTrayIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.lblGunterStatus = new System.Windows.Forms.Label();
            this.btnGunter2Point0 = new System.Windows.Forms.Button();
            this.btnDevelopmentWiki = new System.Windows.Forms.Button();
            this.btnInfrastructureWiki = new System.Windows.Forms.Button();
            this.lblUserName = new System.Windows.Forms.Label();
            this.lblTestNoOfErrors = new System.Windows.Forms.Label();
            this.txtNoOfErrors = new System.Windows.Forms.TextBox();
            this.lblPaymentshieldShieldLogo = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblINT05Status = new System.Windows.Forms.Label();
            this.lblINT06Status = new System.Windows.Forms.Label();
            this.lblINT07Status = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.webGunter2Point0 = new System.Windows.Forms.WebBrowser();
            this.contextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnHideDesktopGunter
            // 
            this.btnHideDesktopGunter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(114)))), ((int)(((byte)(145)))));
            this.btnHideDesktopGunter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnHideDesktopGunter.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnHideDesktopGunter.ForeColor = System.Drawing.Color.White;
            this.btnHideDesktopGunter.Location = new System.Drawing.Point(600, 115);
            this.btnHideDesktopGunter.Name = "btnHideDesktopGunter";
            this.btnHideDesktopGunter.Size = new System.Drawing.Size(87, 28);
            this.btnHideDesktopGunter.TabIndex = 1;
            this.btnHideDesktopGunter.Text = "Hide";
            this.btnHideDesktopGunter.UseVisualStyleBackColor = false;
            this.btnHideDesktopGunter.Click += new System.EventHandler(this.btnHideDesktopGunter_Click);
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.navShowDesktopGunter,
            this.navHideDesktopGunter,
            this.navExitDesktopGunter});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(104, 70);
            // 
            // navShowDesktopGunter
            // 
            this.navShowDesktopGunter.Name = "navShowDesktopGunter";
            this.navShowDesktopGunter.Size = new System.Drawing.Size(103, 22);
            this.navShowDesktopGunter.Text = "Show";
            this.navShowDesktopGunter.Click += new System.EventHandler(this.navShowDesktopGunter_Click);
            // 
            // navHideDesktopGunter
            // 
            this.navHideDesktopGunter.Name = "navHideDesktopGunter";
            this.navHideDesktopGunter.Size = new System.Drawing.Size(103, 22);
            this.navHideDesktopGunter.Text = "Hide";
            this.navHideDesktopGunter.Click += new System.EventHandler(this.navHideDesktopGunter_Click);
            // 
            // navExitDesktopGunter
            // 
            this.navExitDesktopGunter.Name = "navExitDesktopGunter";
            this.navExitDesktopGunter.Size = new System.Drawing.Size(103, 22);
            this.navExitDesktopGunter.Text = "Exit";
            this.navExitDesktopGunter.Click += new System.EventHandler(this.navExitDesktopGunter_Click);
            // 
            // icoGunterSystemTrayIcon
            // 
            this.icoGunterSystemTrayIcon.Text = "niGunterSystemTrayIcon";
            this.icoGunterSystemTrayIcon.Visible = true;
            this.icoGunterSystemTrayIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.niGunterSystemTrayIcon_MouseDoubleClick);
            // 
            // lblGunterStatus
            // 
            this.lblGunterStatus.AutoSize = true;
            this.lblGunterStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(177)))), ((int)(((byte)(76)))));
            this.lblGunterStatus.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGunterStatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblGunterStatus.Location = new System.Drawing.Point(12, 13);
            this.lblGunterStatus.MinimumSize = new System.Drawing.Size(770, 85);
            this.lblGunterStatus.Name = "lblGunterStatus";
            this.lblGunterStatus.Size = new System.Drawing.Size(770, 85);
            this.lblGunterStatus.TabIndex = 3;
            // 
            // btnGunter2Point0
            // 
            this.btnGunter2Point0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(114)))), ((int)(((byte)(145)))));
            this.btnGunter2Point0.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnGunter2Point0.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnGunter2Point0.ForeColor = System.Drawing.Color.White;
            this.btnGunter2Point0.Location = new System.Drawing.Point(694, 115);
            this.btnGunter2Point0.Name = "btnGunter2Point0";
            this.btnGunter2Point0.Size = new System.Drawing.Size(87, 28);
            this.btnGunter2Point0.TabIndex = 4;
            this.btnGunter2Point0.Text = "Gunter 2.0";
            this.btnGunter2Point0.UseVisualStyleBackColor = false;
            this.btnGunter2Point0.Click += new System.EventHandler(this.btnGunter2Point0_Click);
            // 
            // btnDevelopmentWiki
            // 
            this.btnDevelopmentWiki.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(114)))), ((int)(((byte)(145)))));
            this.btnDevelopmentWiki.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnDevelopmentWiki.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnDevelopmentWiki.ForeColor = System.Drawing.Color.White;
            this.btnDevelopmentWiki.Location = new System.Drawing.Point(600, 149);
            this.btnDevelopmentWiki.Name = "btnDevelopmentWiki";
            this.btnDevelopmentWiki.Size = new System.Drawing.Size(181, 28);
            this.btnDevelopmentWiki.TabIndex = 5;
            this.btnDevelopmentWiki.Text = "Developer Wiki";
            this.btnDevelopmentWiki.UseVisualStyleBackColor = false;
            this.btnDevelopmentWiki.Click += new System.EventHandler(this.btnDevelopmentWiki_Click);
            // 
            // btnInfrastructureWiki
            // 
            this.btnInfrastructureWiki.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(114)))), ((int)(((byte)(145)))));
            this.btnInfrastructureWiki.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnInfrastructureWiki.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnInfrastructureWiki.ForeColor = System.Drawing.Color.White;
            this.btnInfrastructureWiki.Location = new System.Drawing.Point(600, 183);
            this.btnInfrastructureWiki.Name = "btnInfrastructureWiki";
            this.btnInfrastructureWiki.Size = new System.Drawing.Size(181, 28);
            this.btnInfrastructureWiki.TabIndex = 6;
            this.btnInfrastructureWiki.Text = "Infrastructure Wiki";
            this.btnInfrastructureWiki.UseVisualStyleBackColor = false;
            this.btnInfrastructureWiki.Click += new System.EventHandler(this.btnInfrastructureWiki_Click);
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(114)))), ((int)(((byte)(145)))));
            this.lblUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.ForeColor = System.Drawing.Color.White;
            this.lblUserName.Location = new System.Drawing.Point(601, 353);
            this.lblUserName.MinimumSize = new System.Drawing.Size(181, 28);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(181, 28);
            this.lblUserName.TabIndex = 7;
            this.lblUserName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTestNoOfErrors
            // 
            this.lblTestNoOfErrors.AutoSize = true;
            this.lblTestNoOfErrors.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(115)))), ((int)(((byte)(157)))), ((int)(((byte)(181)))));
            this.lblTestNoOfErrors.ForeColor = System.Drawing.Color.White;
            this.lblTestNoOfErrors.Location = new System.Drawing.Point(600, 319);
            this.lblTestNoOfErrors.MinimumSize = new System.Drawing.Size(87, 28);
            this.lblTestNoOfErrors.Name = "lblTestNoOfErrors";
            this.lblTestNoOfErrors.Size = new System.Drawing.Size(90, 28);
            this.lblTestNoOfErrors.TabIndex = 9;
            this.lblTestNoOfErrors.Text = "Enter NoOfErrors:";
            this.lblTestNoOfErrors.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtNoOfErrors
            // 
            this.txtNoOfErrors.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNoOfErrors.Location = new System.Drawing.Point(694, 319);
            this.txtNoOfErrors.MinimumSize = new System.Drawing.Size(87, 28);
            this.txtNoOfErrors.Multiline = true;
            this.txtNoOfErrors.Name = "txtNoOfErrors";
            this.txtNoOfErrors.Size = new System.Drawing.Size(87, 28);
            this.txtNoOfErrors.TabIndex = 10;
            this.txtNoOfErrors.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblPaymentshieldShieldLogo
            // 
            this.lblPaymentshieldShieldLogo.AutoSize = true;
            this.lblPaymentshieldShieldLogo.BackColor = System.Drawing.Color.White;
            this.lblPaymentshieldShieldLogo.CausesValidation = false;
            this.lblPaymentshieldShieldLogo.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.lblPaymentshieldShieldLogo.Location = new System.Drawing.Point(20, 20);
            this.lblPaymentshieldShieldLogo.MinimumSize = new System.Drawing.Size(70, 70);
            this.lblPaymentshieldShieldLogo.Name = "lblPaymentshieldShieldLogo";
            this.lblPaymentshieldShieldLogo.Size = new System.Drawing.Size(70, 70);
            this.lblPaymentshieldShieldLogo.TabIndex = 11;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.ForeColor = System.Drawing.Color.White;
            this.lblStatus.Location = new System.Drawing.Point(470, 20);
            this.lblStatus.MinimumSize = new System.Drawing.Size(300, 70);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(300, 70);
            this.lblStatus.TabIndex = 12;
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblINT05Status
            // 
            this.lblINT05Status.AutoSize = true;
            this.lblINT05Status.BackColor = System.Drawing.Color.Green;
            this.lblINT05Status.Location = new System.Drawing.Point(694, 217);
            this.lblINT05Status.MinimumSize = new System.Drawing.Size(87, 28);
            this.lblINT05Status.Name = "lblINT05Status";
            this.lblINT05Status.Size = new System.Drawing.Size(87, 28);
            this.lblINT05Status.TabIndex = 16;
            // 
            // lblINT06Status
            // 
            this.lblINT06Status.AutoSize = true;
            this.lblINT06Status.BackColor = System.Drawing.Color.Green;
            this.lblINT06Status.Location = new System.Drawing.Point(694, 251);
            this.lblINT06Status.MinimumSize = new System.Drawing.Size(87, 28);
            this.lblINT06Status.Name = "lblINT06Status";
            this.lblINT06Status.Size = new System.Drawing.Size(87, 28);
            this.lblINT06Status.TabIndex = 17;
            // 
            // lblINT07Status
            // 
            this.lblINT07Status.AutoSize = true;
            this.lblINT07Status.BackColor = System.Drawing.Color.Green;
            this.lblINT07Status.Location = new System.Drawing.Point(694, 285);
            this.lblINT07Status.MinimumSize = new System.Drawing.Size(87, 28);
            this.lblINT07Status.Name = "lblINT07Status";
            this.lblINT07Status.Size = new System.Drawing.Size(87, 28);
            this.lblINT07Status.TabIndex = 18;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(114)))), ((int)(((byte)(145)))));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(600, 217);
            this.label1.MinimumSize = new System.Drawing.Size(87, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 28);
            this.label1.TabIndex = 19;
            this.label1.Text = "INT-05";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(114)))), ((int)(((byte)(145)))));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(601, 251);
            this.label2.MinimumSize = new System.Drawing.Size(87, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 28);
            this.label2.TabIndex = 20;
            this.label2.Text = "INT-06";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(114)))), ((int)(((byte)(145)))));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(601, 285);
            this.label3.MinimumSize = new System.Drawing.Size(87, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 28);
            this.label3.TabIndex = 21;
            this.label3.Text = "INT-07";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // webGunter2Point0
            // 
            this.webGunter2Point0.AllowNavigation = false;
            this.webGunter2Point0.Location = new System.Drawing.Point(12, 115);
            this.webGunter2Point0.MinimumSize = new System.Drawing.Size(20, 20);
            this.webGunter2Point0.Name = "webGunter2Point0";
            this.webGunter2Point0.ScrollBarsEnabled = false;
            this.webGunter2Point0.Size = new System.Drawing.Size(574, 470);
            this.webGunter2Point0.TabIndex = 2;
            this.webGunter2Point0.Url = new System.Uri("http://gunter2/", System.UriKind.Absolute);
            // 
            // DesktopGunterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(797, 600);
            this.ContextMenuStrip = this.contextMenu;
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblINT07Status);
            this.Controls.Add(this.lblINT06Status);
            this.Controls.Add(this.lblINT05Status);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.lblPaymentshieldShieldLogo);
            this.Controls.Add(this.txtNoOfErrors);
            this.Controls.Add(this.lblTestNoOfErrors);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.btnInfrastructureWiki);
            this.Controls.Add(this.btnDevelopmentWiki);
            this.Controls.Add(this.btnGunter2Point0);
            this.Controls.Add(this.lblGunterStatus);
            this.Controls.Add(this.webGunter2Point0);
            this.Controls.Add(this.btnHideDesktopGunter);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DesktopGunterForm";
            this.Text = "DesktopGunter 0.1";
            this.contextMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnHideDesktopGunter;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem navShowDesktopGunter;
        private System.Windows.Forms.ToolStripMenuItem navHideDesktopGunter;
        private System.Windows.Forms.ToolStripMenuItem navExitDesktopGunter;
        private System.Windows.Forms.NotifyIcon icoGunterSystemTrayIcon;
        private System.Windows.Forms.Label lblGunterStatus;
        private System.Windows.Forms.Button btnGunter2Point0;
        private System.Windows.Forms.Button btnDevelopmentWiki;
        private System.Windows.Forms.Button btnInfrastructureWiki;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label lblTestNoOfErrors;
        private System.Windows.Forms.TextBox txtNoOfErrors;
        private System.Windows.Forms.Label lblPaymentshieldShieldLogo;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label lblINT05Status;
        private System.Windows.Forms.Label lblINT06Status;
        private System.Windows.Forms.Label lblINT07Status;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.WebBrowser webGunter2Point0;
    }
}

